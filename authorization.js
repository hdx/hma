/*
 *  Generic require login routing middleware
 */

exports.requiresLogin = function (req, res, next) {
    if (!req.loggedIn) {
        req.flash('notice', 'You are not authorized. Please login');
        res.redirect('/');
    } else {
        next();
    }
};

exports.requiresAdmin = function(req, res, next) {
    if(!req.user.isAdmin) {
        req.flash('notice', 'You are not authorized. Please login with an admin account');
        res.redirect('/');
    } else {
        next();
    }
};

/*
 *  User authorizations routing middleware
 */

exports.user = {
    hasAuthorization : function (req, res, next) {
        if (req.foundUser.id != req.session.auth.userId) {
            req.flash('notice', 'You are not authorized');
            res.redirect('/profile/'+req.foundUser.id);
        } else {
            next();
        }
    }
};

/*
 *  Article authorizations routing middleware
 */

exports.article = {
    hasAuthorization : function (req, res, next) {
        if (req.article.user._id.toString() != req.user._id.toString()) {
            req.flash('notice', 'You are not authorized');
            res.redirect('/article/'+req.article.id);
        } else {
            next();
        }
    }
};

exports.helpDocument = {
    hasAuthorization : function (req, res, next) {
        if (req.helpDocument.user._id.toString() != req.user._id.toString()) {
            req.flash('notice', 'You are not authorized');
            res.redirect('/helpDocument/'+req.helpDocument.id);
        } else {
            next();
        }
    }
};

exports.page = {
    hasAuthorization : function (req, res, next) {
        if (req.page.user._id.toString() != req.user._id.toString()) {
            req.flash('notice', 'You are not authorized');
            res.redirect('/pages');
        } else {
            next();
        }
    }
};

exports.suggestion = {
    hasAuthorization : function (req, res, next) {
        if (req.suggestion.user._id.toString() != req.user._id.toString()) {
            req.flash('notice', 'You are not authorized');
            res.redirect('/suggestion/'+req.suggestion.id);
        } else {
            next();
        }
    }
};