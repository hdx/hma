// ClickPoint schema

var ClickPointSchema = new Schema({
    point       : {x : Number, y:Number},
    page        : {type : Schema.ObjectId, ref : 'Page'},
    created_at  : {type : Date, default : Date.now}
});

mongoose.model('ClickPoint', ClickPointSchema);
