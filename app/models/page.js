// Article schema

var PageSchema = new Schema({
    title       : {type : String, default : '', trim : true},
    description        : {type : String, default : '', trim : true},
    url        : {type : String, default : '', trim : true},
    user        : {type : Schema.ObjectId, ref : 'User'},
    created_at  : {type : Date, default : Date.now}
});

PageSchema.path('title').validate(function (title) {
  return title.length > 0
}, 'Page title cannot be blank');

PageSchema.path('url').validate(function (url) {
  return url.length > 0
}, 'Page URL cannot be blank');

mongoose.model('Page', PageSchema);
