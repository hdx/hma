var exports = module.exports = everyauth = require('everyauth'),
    Promise = everyauth.Promise,
    UserSchema = new Schema({}),
    User;

var exports = module.exports = mongooseAuth = require('mongoose-auth');

everyauth.debug = true;

everyauth.password.loginLocals({
    title: 'Login'
});

everyauth.password.registerLocals({
    title: 'Register'
});

// To see all the configurable options
// console.log(everyauth.facebook.configurable())
//console.log(everyauth.password.configurable());

UserSchema.add({
    created_at  : {type : Date, default : Date.now},
    roles: {type : [String], default: ["default"]}
});

UserSchema.plugin(mongooseAuth, {
    everymodule: {
        everyauth: {
            User: function () {
                return User
            }
        }
    },
    password: {
        loginWith: 'email',
        extraParams: {
            email: String,
            name: String
        },
        everyauth: {
            getLoginPath: '/login',
            postLoginPath: '/login',
            loginView: 'login.jade',
            getRegisterPath: '/register',
            postRegisterPath: '/register',
            registerView: 'register.jade',
            loginSuccessRedirect: '/',
            registerSuccessRedirect: '/',
            validateRegistration: function (newUserAttrs, errors) {
                var promise = this.Promise()
                    , User = this.User()()
                    , user = new User(newUserAttrs);
                user.validate( function (err) {
                    if (err) {
                        if(typeof err.errors === 'object')  {
                            for(var customValidationErrorKey in err.errors) {
                                if(err.errors.hasOwnProperty(customValidationErrorKey)) {
                                    errors.push(err.errors[customValidationErrorKey].type)
                                }
                            }
                        }
                        // errors.push(err.message || err);
                    }
                    if (errors.length)
                        return promise.fulfill(errors);
                    promise.fulfill(null);
                });
                return promise;
            }
        }
    }
});

UserSchema.virtual('isAdmin').get(function(){
    return this.roles.indexOf("admin") !== -1;
});

// validations

UserSchema.path('name').validate(function (name) {
    return name.trim().split(' ').length >= 2
}, 'Please provide your full name');

var exports = module.exports = User = mongoose.model('User', UserSchema);
