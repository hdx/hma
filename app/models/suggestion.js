// Suggestion schema

var SuggestionSchema = new Schema({
    title       : {type : String, default : '', trim : true},
    body        : {type : String, default : '', trim : true},
    user        : {type : Schema.ObjectId, ref : 'User'},
    votes       : {type:  Number, default: 0},
    created_at  : {type : Date, default : Date.now}
});

SuggestionSchema.path('title').validate(function (title) {
    return title.length > 0
}, 'Suggestion title cannot be blank');

SuggestionSchema.path('body').validate(function (body) {
    return body.length > 0
}, 'Suggestion body cannot be blank');

mongoose.model('Suggestion', SuggestionSchema);
