// HelpDocument schema

var HelpDocumentSchema = new Schema({
    title       : {type : String, default : '', trim : true}
  , body        : {type : String, default : '', trim : true}
  , user        : {type : Schema.ObjectId, ref : 'User'}
  , created_at  : {type : Date, default : Date.now}
});

HelpDocumentSchema.path('title').validate(function (title) {
  return title.length > 0
}, 'HelpDocument title cannot be blank');

HelpDocumentSchema.path('body').validate(function (body) {
  return body.length > 0
}, 'HelpDocument body cannot be blank');

mongoose.model('HelpDocument', HelpDocumentSchema);
