var Suggestion = mongoose.model('Suggestion'),
    utils = require("../../lib/utils");

module.exports = function(app, auth){

    // New suggestion
    app.get('/suggestions/new', auth.requiresLogin, function(req, res){
        res.render('suggestions/new', {
            title: 'New Suggestion',
            suggestion: new Suggestion({})
        })
    });

    app.param('suggestionId', function(req, res, next, id){
        Suggestion
            .findOne({ _id : req.params.suggestionId })
            .populate('user')
            .exec(function(err,suggestion) {
                if (err) return next(err) ;
                if (!suggestion) return next(new Error('Failed to load suggestion ' + id));
                req.suggestion = suggestion ;
                next();
            });
    }) ;

    // Create an suggestion
    app.post('/suggestions', auth.requiresLogin, function(req, res){
        var suggestion = new Suggestion(req.body.suggestion);
        suggestion.user = req.session.auth.userId;

        suggestion.save(function(err){
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('suggestions/new', {
                    title: 'New Suggestion'
                    , suggestion: suggestion
                });
            }
            else {
                req.flash('notice', 'Created successfully');
                res.redirect('/suggestion/'+suggestion._id);
            }
        })
    });

    // Edit an suggestion
    app.get('/suggestion/:suggestionId/edit', auth.requiresLogin, auth.requiresAdmin, function(req, res){
        res.render('suggestions/edit', {
            title: 'Edit '+req.suggestion.title,
            suggestion: req.suggestion
        })
    });

    // Update suggestion
    app.put('/suggestions/:suggestionId', auth.requiresLogin, auth.requiresAdmin, function(req, res){
        var suggestion = req.suggestion;

        suggestion.title = req.body.suggestion.title;
        suggestion.body = req.body.suggestion.body;

        suggestion.save(function(err, doc) {
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('suggestions/edit', {
                    title: 'Edit Suggestion'
                    , suggestion: suggestion
                })
            }
            else {
                req.flash('notice', 'Updated successfully');
                res.redirect('/suggestion/'+suggestion._id)
            }
        })
    });

    //vote up
    app.post('/suggestions/vote-up/:suggestionId', auth.requiresLogin, function(req, res){
        var suggestion = req.suggestion;

        suggestion.votes = suggestion.votes+1;

        res.contentType('json');

        suggestion.save(function(err, doc) {
            if (err) {
                res.send({ response:'error', message: 'Vote failed :('});
            }
            else {
                res.send({response:'success', votes: doc.votes});
            }
        });
    });

    // View an suggestion
    app.get('/suggestion/:suggestionId', function(req, res){
        Suggestion
            .find({})
            .populate('user')
            .sort('-votes') // sort by date
            .exec(function(err, suggestions) {
                if (err) throw err;
                res.render('suggestions/show', {
                    title: req.suggestion.title,
                    suggestion: req.suggestion,
                    suggestions: suggestions
                });
            });

    });

    // Delete an suggestion
    app.del('/suggestion/:suggestionId', auth.requiresLogin, auth.requiresAdmin, function(req, res){
        var suggestion = req.suggestion;
        suggestion.remove(function(err){
            // req.flash('notice', 'Deleted successfully')
            res.redirect('/suggestions')
        })
    });

    // Listing of Suggestions
    app.get('/suggestions', function(req, res){
        Suggestion
            .find({})
            .populate('user')
            .sort('-votes') // sort by date
            .exec(function(err, suggestions) {
                if (err) throw err;
                res.render('suggestions/index', {
                    title: 'List of Suggestions',
                    suggestions: suggestions
                })
            })
    });
};
