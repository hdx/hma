var Page = mongoose.model('Page'),
    utils = require("../../lib/utils");

module.exports = function(app, auth){

    // New page
    app.get('/pages/new', auth.requiresLogin, function(req, res){
        res.render('pages/new', {
            title: 'New Page',
            page: new Page({})
        });
    });

    app.param('pageId', function(req, res, next, id){
        Page
            .findOne({ _id : req.params.pageId })
            .populate('user')
            .exec(function(err,page) {
                if (err) return next(err) ;
                if (!page) return next(new Error('Failed to load page ' + id));
                req.page = page ;
                next();
            });
    }) ;

    // Create an page
    app.post('/pages', function(req, res){
        var page = new Page(req.body.page);
        page.user = req.session.auth.userId;

        page.save(function(err){
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('pages/new', {
                    title: 'New Page'
                    , page: page
                });
            }
            else {
                req.flash('notice', 'Created successfully');
                res.redirect('/page/'+page._id);
            }
        })
    });

    // Edit an page
    app.get('/page/:pageId/edit', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        res.render('pages/edit', {
            title: 'Edit '+req.page.title,
            page: req.page
        })
    });

    // Update page
    app.put('/pages/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var page = req.page;

        page.title = req.body.page.title;
        page.url = req.body.page.url;
        page.description = req.body.page.description;

        page.save(function(err, doc) {
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('pages/edit', {
                    title: 'Edit Page'
                    , page: page
                })
            }
            else {
                req.flash('notice', 'Updated successfully');
                res.redirect('/page/'+page._id)
            }
        });
    });

    // View an page
    app.get('/page/:pageId', auth.page.hasAuthorization, function(req, res){
        Page
            .find({user:req.user})
            .populate('user')
            .sort('-created_at') // sort by date
            .exec(function(err, pages) {
                if (err) throw err;
                res.render('pages/show', {
                    title: req.page.title,
                    page: req.page,
                    pages: pages
                });
            });
    });

    // Delete an page
    app.del('/page/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var page = req.page;
        page.remove(function(err){
            // req.flash('notice', 'Deleted successfully')
            res.redirect('/pages')
        })
    });

    // Listing of Pages
    app.get('/pages', auth.requiresLogin, function(req, res){
        Page
            .find({user:req.user})
            .populate('user')
            .sort('-created_at') // sort by date
            .exec(function(err, pages) {
                if (err) throw err;
                res.render('pages/index', {
                    title: 'List Pages',
                    pages: pages
                })
            })
    });
};
