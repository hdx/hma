var HelpDocument = mongoose.model('HelpDocument'),
    utils = require("../../lib/utils");

module.exports = function(app, auth){

    // New helpDocument
    app.get('/helpDocuments/new', auth.requiresLogin, auth.requiresAdmin, function(req, res){
        res.render('help_documents/new', {
            title: 'New Help Document',
            helpDocument: new HelpDocument({})
        })
    });

    app.param('helpDocumentId', function(req, res, next, id){
        HelpDocument
            .findOne({ _id : req.params.helpDocumentId })
            .populate('user')
            .exec(function(err,helpDocument) {
                if (err) return next(err) ;
                if (!helpDocument) return next(new Error('Failed to load helpDocument ' + id));
                req.helpDocument = helpDocument ;
                next();
            });
    }) ;

    // Create an helpDocument
    app.post('/helpDocuments', function(req, res){
        var helpDocument = new HelpDocument(req.body.helpDocument);
        helpDocument.user = req.session.auth.userId;

        helpDocument.save(function(err){
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('help_documents/new', {
                    title: 'New Help Document'
                    , helpDocument: helpDocument
                });
            }
            else {
                req.flash('notice', 'Created successfully');
                res.redirect('/helpDocument/'+helpDocument._id);
            }
        })
    });

    // Edit an helpDocument
    app.get('/helpDocument/:helpDocumentId/edit', auth.requiresLogin, auth.helpDocument.hasAuthorization, function(req, res){
        res.render('help_documents/edit', {
            title: 'Edit '+req.helpDocument.title,
            helpDocument: req.helpDocument
        })
    });

    // Update helpDocument
    app.put('/helpDocuments/:helpDocumentId', auth.requiresLogin, auth.helpDocument.hasAuthorization, function(req, res){
        var helpDocument = req.helpDocument;

        helpDocument.title = req.body.helpDocument.title;
        helpDocument.body = req.body.helpDocument.body;

        helpDocument.save(function(err, doc) {
            if (err) {
                utils.mongooseErrorHandler(err, req);
                res.render('help_documents/edit', {
                    title: 'Edit HelpDocument'
                    , helpDocument: helpDocument
                })
            }
            else {
                req.flash('notice', 'Updated successfully');
                res.redirect('/helpDocument/'+helpDocument._id)
            }
        })
    });

    // View an helpDocument
    app.get('/helpDocument/:helpDocumentId', function(req, res){
        HelpDocument
            .find({})
            .populate('user')
            .sort('-created_at') // sort by date
            .exec(function(err, helpDocuments) {
                if (err) throw err;
                res.render('help_documents/show', {
                    title: req.helpDocument.title,
                    helpDocument: req.helpDocument,
                    helpDocuments: helpDocuments
                });
            });
    });

    // Delete an helpDocument
    app.del('/helpDocument/:helpDocumentId', auth.requiresLogin, auth.helpDocument.hasAuthorization, function(req, res){
        var helpDocument = req.helpDocument;
        helpDocument.remove(function(err){
            // req.flash('notice', 'Deleted successfully')
            res.redirect('/helpDocuments')
        })
    });

    // Listing of HelpDocuments
    app.get('/helpDocuments', function(req, res){
        HelpDocument
            .find({})
            .populate('user')
            .sort('-created_at') // sort by date
            .exec(function(err, helpDocuments) {
                if (err) throw err;
                res.render('help_documents/index', {
                    title: 'Help Documents',
                    helpDocuments: helpDocuments
                })
            })
    });
};
