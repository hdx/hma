var User = mongoose.model('User');

module.exports = function (app, auth) {

    app.param('profileId', function (req, res, next, id) {
        User
            .findOne({ _id : id })
            .exec(function (err, user) {
                if (err) return next(err);
                if (!user) return next(new Error('Failed to load User ' + id));
                req.foundUser = user;
                next();
            });
    });

    // Handles session Logout
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/')
    });

// Listing of Users
    app.get('/users', auth.requiresLogin, auth.requiresAdmin, function(req, res){
        User
            .find({})
            .sort('name')
            .exec(function(err, users) {
                if (err) throw err;
                res.render('users/index', {
                    title: 'List of Users',
                    users: users
                })
            })
    });




    // Profile view
    app.get('/profile/:profileId', function (req, res) {
        var user1 = req.foundUser;
        res.render('users/profile', {
            title : user1.name
            , user1 : user1
        })
    })
};
