var Page = mongoose.model('Page'),
    MovePoint = mongoose.model('MovePoint'),
    ClickPoint = mongoose.model('ClickPoint'),
    utils = require("../../lib/utils");

module.exports = function(app, auth){
    app.get('/maps/move/:pageId', auth.requiresLogin, auth.page.hasAuthorization,  function(req, res){
        res.render('heatmap', {
            title: 'MoveMap',
            page: req.page,
            streamType: "Move"
        });
    });
    app.get('/maps/click/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        res.render('heatmap', {
            title: 'ClickMap',
            page: req.page,
            streamType: "Click"
        });
    });

    var mapReduceOptions = {};
    mapReduceOptions.map = function () { emit(this.point, {x:this.point.x, y:this.point.y, count: 1}) };
    mapReduceOptions.reduce = function (k, vals) {
        var retVal = {x: k.x, y:k.y, count: 0};
        vals.forEach(function(value) {
            retVal.count += value.count;
        });
        return retVal;
    };

    function buildResult(mapReduceResult) {
        var result = {max: 1, data:[]};

        mapReduceResult.forEach(function(mrResult){
            result.data.push(mrResult.value);
            if(result.max < mrResult.value.count) {
                result.max = mrResult.value.count;
            }
        });

        return result;
    }


    app.get('/maps/click/lastDay/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();

        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 24*60*60*1000)}};
        ClickPoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });

    app.get('/maps/click/lastWeek/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();

        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 7*24*60*60*1000)}};
        ClickPoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });

    app.get('/maps/click/lastMonth/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();
        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 30*24*60*60*1000)}};
        ClickPoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });

    app.get('/maps/move/lastDay/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();

        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 24*60*60*1000)}};
        MovePoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });

    app.get('/maps/move/lastWeek/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();

        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 7*24*60*60*1000)}};
        MovePoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });

    app.get('/maps/move/lastMonth/:pageId', auth.requiresLogin, auth.page.hasAuthorization, function(req, res){
        var currentDate = new Date();
        mapReduceOptions.query = {page: req.page, created_at:{$gt: currentDate.setTime(currentDate.getTime() - 30*24*60*60*1000)}};
        MovePoint.mapReduce(mapReduceOptions, function (err, results) {
            res.send(buildResult(results));
        });
    });
};
