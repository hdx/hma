module.exports = function(app, auth){
    // home
    app.get('/', function(req, res){
        res.render('home', {
            title: 'Home Page'
        });
    });

    app.get('/features',function(req,res){
        res.render('features',{
            title:"Features"
        });
    });

    app.get('/pricing',function(req,res){
        res.render('pricing',{
            title:"Pricing"
        });
    });
};
