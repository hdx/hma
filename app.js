/* Main application entry file. Please note, the order of loading is important.
 * Configuration loading and booting of controllers and custom error handlers */

var express = require('express'),
    fs = require('fs'),
    utils = require('./lib/utils'),
    auth = require('./authorization');

// Load configurations
var config_file = require('yaml-config');
config = config_file.readConfig('config/config.yaml');

require('./db-connect');                // Bootstrap db connection

// Bootstrap models
var models_path = __dirname + '/app/models'
    , model_files = fs.readdirSync(models_path);
model_files.forEach(function (file) {
    if (file == 'user.js')
        User = require(models_path+'/'+file);
    else
        require(models_path+'/'+file)
});

var app = express.createServer();       // express app
require('./settings').boot(app);        // Bootstrap application settings

// Bootstrap controllers
var controllers_path = __dirname + '/app/controllers'
    , controller_files = fs.readdirSync(controllers_path);
controller_files.forEach(function (file) {
    require(controllers_path+'/'+file)(app, auth)
});

require('./error-handler').boot(app);   // Bootstrap custom error handler
mongooseAuth.helpExpress(app);          // Add in Dynamic View Helpers
everyauth.helpExpress(app, { userAlias: 'current_user' });

// Start the app by listening on <port>
var port = process.env.PORT || 3000;
app.listen(port);

// Start Socket.IO
var io = require('socket.io').listen(app);

var MovePoint = mongoose.model('MovePoint');

var ClickPoint = mongoose.model('ClickPoint');

io.sockets.on('connection', function (socket) {

    socket.on('subscribeToMoveStream', function(data) {
        socket.join(data.pageId+"_move");
    });

    socket.on('subscribeToClickStream', function(data) {
        socket.join(data.pageId+"_click");
    });

    socket.on('unSubscribeToMoveStream', function(data) {
        socket.leave(data.pageId+"_move");
    });

    socket.on('unSubscribeToClickStream', function(data) {
        socket.leave(data.pageId+"_click");
    });

    socket.on('trackMove', function (data) {
        var newPoint = new MovePoint({
            page: mongoose.Types.ObjectId(data.pageId),
            point: data.point
        });
        newPoint.save(function(err){
            //console.log("new move point saved!")
        });
        io.sockets.in(data.pageId+"_move").emit('addToHeatMap', data);
        //socket.emit("addToHeatMap", data);
    });

    socket.on('trackClick', function (data) {
        var newPoint = new ClickPoint({
            page: mongoose.Types.ObjectId(data.pageId),
            point: data.point
        });
        newPoint.save(function(err){
            //console.log("new click point saved!")
        });
        io.sockets.in(data.pageId+"_click").emit('addToHeatMap', data);
    });
});

console.log('Express app started on port '+port);
