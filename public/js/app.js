$(function() {

    //suggestion voting
    $("#votes-container button").click(function(e){
        var suggestionId = $(this).data('suggestion-id');
        var that = this;
        $.ajax({
            url: "/suggestions/vote-up/"+suggestionId,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            cache: false,
            timeout: 5000,
            success: function(data) {
                console.log(data);

                if(data.response === 'success') {
                    console.log($(".votes", $(that)));
                    $("#votes-container .votes").html(data.votes + " Votes")
                } else {
                    console.warn("Error Voting")
                }
            },

            error: function() {
                console.log('process error');
            }
        });
    });

});